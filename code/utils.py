from typing import Optional

import numpy as np

SEED = 123456789


def generate_users(n_users: int, radius: float) -> np.ndarray:
    """Generates n_users in a circle of radius r (in meter)

    Parameters
    ----------
    n_users : int
        Number of users to generate
    radius : float
        radius of the cell to generate the users in

    Returns
    -------
    np.ndarray (n_users, 2)
        Array of 2D positions (one line per user)
    """

    # set the seed (arbitrarily) for reproducibility
    rng = np.random.default_rng(seed=SEED)

    theta = rng.uniform(0, 2 * np.pi, n_users)
    rho = radius * np.sqrt(rng.uniform(0, 1, n_users))

    x = rho * np.cos(theta)
    y = rho * np.sin(theta)

    return np.stack([x, y]).transpose()


def generate_channel(users: np.ndarray, fading_rate: float, fading: Optional[float] = None) -> np.ndarray:
    """Generate the channel gain for the users

    Parameters
    ----------
    users : np.ndarray (n_users, 2)
        The positions of the users
    fading_rate : float
        The average fading rate
    fading : float
        If provided, sets the same fading value for all users, ignoring
        fading_rate. Optional.

    Returns
    -------
    np.ndarray (n_users,)
        The channel coefficient for each user
    """
    # set the seed (arbitrarily) for reproducibility
    rng = np.random.default_rng(seed=SEED)

    distance = np.sqrt(users[:, 0]**2 + users[:, 1]**2)
    pathloss = 10**(-12.8 + -3.76 * np.log10(distance))

    if fading:
        fading = fading * np.ones(users.shape[0])

    else:
        fading = rng.exponential(1/fading_rate, users.shape[0])

    return pathloss * fading


def cartesian(q: int, n: int) -> np.ndarray:
    """
    Generate the cartesian product [0, q)^n.
    """

    return np.array(
        np.meshgrid(*[np.arange(q) for _ in range(n)])
    ).T.reshape(-1, n)
