from pathlib import Path
import sched
from typing import Callable

import tqdm
import numpy as np
from scipy.stats import bernoulli
from matplotlib import pyplot as pl

from utils import cartesian, generate_channel, generate_users, SEED

CELL_RADIUS = 1  # r (in km)
FADING_RATE = 1/2  # \lambda
NOISE_POWER = 10**((-104 - 30) / 10)  # \sigma^2
ANTENNA_POWER = 10**((40 - 30) / 10)  # P
N_ITERATIONS = 100_000
PENALTY = 1
# RATE_THREASHOLD = 1 / PENALTY
RATE_THREASHOLD = .99

png_fig = Path('./figures/')
pgf_fig = Path('../report/figures/')
png_fig.mkdir(exist_ok=True, parents=True)
pgf_fig.mkdir(exist_ok=True, parents=True)

pl.rcParams.update({
    # "figure.figsize": [.6*6.4, .6*4.8],     # change figure default size
    "figure.figsize": [6.4, 4.8],     # change figure default size
    "savefig.bbox": "tight",                # image fitted to the figure
    # grid lines for major and minor ticks
    "axes.grid.which": "both",
    "axes.grid": True,                      # always display the grid
    # "lines.marker": "+",                    # add a marker to each point
    # "lines.linewidth": .7,                  # reduce linewidth to better see the points
    "font.family": "serif",                 # use serif/main font for text elements
    "text.usetex": True,                    # use inline math for ticks
    "pgf.rcfonts": False,                   # don't setup fonts from rc parameters
    "pgf.preamble": "\n".join([
        r"\usepackage{url}",                # load additional packages
        r"\usepackage{unicode-math}",       # unicode math setup
        r"\setmainfont{DejaVu Serif}",      # serif font via preamble
    ])
})

COLORS = pl.rcParams['axes.prop_cycle'].by_key()['color']


################################################################################
# QUESTION 2                                                                   #
################################################################################
def vqueue_solve(
    n_iterations: int,
    dim: int,
    state: Callable[[int], np.ndarray],
    variable: Callable[[int, np.ndarray], float],
    constraints: Callable[[int, np.ndarray], np.ndarray],
    next_action: Callable[[np.ndarray, np.ndarray], int],
) -> tuple[np.ndarray, np.ndarray]:
    """Solve a stochastic problem using Lyapunov Optimization on virtual queues

    Parameters
    ----------
    n_iterations : int
        The number of iterations of the solver
    dim : int
        The dimention of the state, (also the dimension of the action, the
        number of constraints and the number of queues)
    state : Callable[[int], np.ndarray (dim, )]
        Function such that current_state = state(time)
    variable : Callable[[int, np.ndarray (dim, )], float]
        Function such that variable_value = variable(time, current_state)
    constraints : Callable[[int, np.ndarray (dim, )], np.ndarray (dim, )]
        Function specifying the constraints : constraints(time, current_state)
        <= 0
    next_action : Callable[[np.ndarray, np.ndarray], int]
        Function returning the next action given the queues and state: action =
        next_action(queue, current_state)

    Returns
    -------
    tuple[np.ndarray, np.ndarray]
        The variable and constraints iterates (values of variable and constraints at each time)
    """

    # Initial values
    queue = np.zeros(dim)
    action = np.zeros(dim)

    iterates = np.zeros(n_iterations)
    constraints_log = np.zeros((n_iterations, dim))

    for t in range(n_iterations):
        # Compute the length of the queues
        state_value = state(action)
        queue = np.maximum(0, queue + constraints(action, state_value))

        # Compute the next action
        action = next_action(queue, state_value)

        # Log the value of the variable
        iterates[t] = variable(action, state_value)
        constraints_log[t] = constraints(action, state_value)

    return iterates, constraints_log


def allocate_user_tdma(queue: np.ndarray, state: np.ndarray, penalty: float) -> np.ndarray:
    """Returns optimal TDMA power allocation given the users' queue lengths and
    current channel state

    By definition of TDMA, only one user is scheduled at each time.

    Parameters
    ----------
    queue : np.ndarray (n_users, )
        The users' (virtual) queues
    state : np.ndarray (n_users, )
        The users' channels
    penalty : float
        The penalty factor of the optimized variable

    Returns
    -------
    np.ndarray (n_users, )
        The power allocated to each user
    """

    # Compute the optimal power for each user assuming we choose to allocate
    # only this user
    optimal_power = np.maximum(
        np.minimum(
            ANTENNA_POWER,
            (queue / penalty) - (NOISE_POWER / state)
        ),
        0
    )

    # Find the user with the lowest allocated power
    allocated_user = np.argmin(
        PENALTY * optimal_power - queue *
        np.log2(1 + optimal_power * state / NOISE_POWER)
    )

    # Compute the power allocation
    allocation = np.zeros_like(queue)
    allocation[allocated_user] = optimal_power[allocated_user]

    return allocation


################################################################################
# QUESTION 1 AND 3                                                             #
################################################################################
def question_1_3():
    rng = np.random.default_rng(seed=SEED)
    N_USERS_VALUES = (20, 30, 50)
    PENALTY_FACTOR_VALUES = np.array([0.1, 0.5, 1, 5, 10, 100])

    # Figures
    users_fig, users_axes = pl.subplots(1, len(N_USERS_VALUES), figsize=(8, 3))
    avg_rate_fig, avg_rate_ax = pl.subplots()
    constraints_fig, constraints_ax = pl.subplots()
    avg_power_fig, avg_power_ax = pl.subplots()
    penalty_fig, penalty_ax = pl.subplots()

    for ax, n_users, color in zip(users_axes.flatten(), N_USERS_VALUES, COLORS):
        print(f'SIMULATION FOR {n_users} USERS')
        users = generate_users(n_users, CELL_RADIUS)

        # Display the user distribution in the cell
        ax.set_aspect('equal')
        ax.set_xlim(-1.05*CELL_RADIUS, 1.05*CELL_RADIUS)
        ax.set_ylim(-1.05*CELL_RADIUS, 1.05*CELL_RADIUS)
        ax.set_xlabel("$x$")
        ax.set_ylabel("$y$")
        t = ax.text(-.75, .75, f'$K = {n_users}$')
        t.set_bbox(dict(facecolor='orange', alpha=1, edgecolor='orange'))

        ax.scatter(users[:, 0], users[:, 1])

        # Assign a rate constraint to each user
        rate = rng.uniform(.5/n_users, 1/n_users, n_users)

        # Run the Lyapunov Optimization for a given number of iterations
        iterates, constraints = vqueue_solve(
            N_ITERATIONS,
            n_users,
            state=lambda time: generate_channel(
                users, fading_rate=FADING_RATE),
            variable=lambda power, channel: np.sum(power),
            constraints=lambda power, channel: (
                rate - np.log2(1 + power * channel / NOISE_POWER)
            ),
            next_action=lambda queue, state: allocate_user_tdma(
                queue, state, PENALTY),
        )

        # Compute the average user rate of each user at each iteration
        # (avg_user_rate[iteration, user])
        avg_user_rate = np.cumsum(
            rate - constraints,
            axis=0
        ) / np.arange(1, N_ITERATIONS + 1).reshape((N_ITERATIONS, 1))

        # Compute the average user rate (averaged over the users) at each iteration
        avg_rate = np.mean(avg_user_rate, axis=1)

        # Compute the percentage of users whose average rate constraint is met
        constraint_met = np.count_nonzero(
            avg_user_rate >= RATE_THREASHOLD * rate, axis=1) / n_users

        # Compute the average total power (avg_power[iteration])
        avg_power = np.cumsum(iterates) / np.arange(1, N_ITERATIONS + 1)

        constraints_ax.semilogx(constraint_met, c=color,
                                label=f'K = {n_users}')
        avg_rate_ax.loglog(avg_rate, c=color, label=f'K = {n_users}')
        avg_rate_ax.hlines(
            np.mean(rate),
            0, N_ITERATIONS,
            ls='--',
            color=color,
            label=f'K = {n_users}, target'
        )
        avg_power_ax.loglog(avg_power, c=color, label=f'K = {n_users}')

        # Compute the optimal average power for differente values of the penalty
        # factor V
        avg_opt_power = np.zeros(PENALTY_FACTOR_VALUES.shape[0])

        for i, penalty_factor in enumerate(PENALTY_FACTOR_VALUES):
            # Run the Lyapunov Optimization for a given number of iterations
            iterates, constraints = vqueue_solve(
                N_ITERATIONS,
                n_users,
                state=lambda time: generate_channel(
                    users, fading_rate=FADING_RATE),
                variable=lambda power, channel: np.sum(power),
                constraints=lambda power, channel: (
                    rate - np.log2(1 + power * channel / NOISE_POWER)
                ),
                next_action=lambda queue, state: allocate_user_tdma(
                    queue, state, penalty_factor),
            )

            # Compute the average total power (avg_power[iteration])
            avg_power = np.cumsum(iterates) / np.arange(1, N_ITERATIONS + 1)
            avg_opt_power[i] = np.mean(avg_power[-100:])

        penalty_ax.loglog(
            PENALTY_FACTOR_VALUES[:-1],
            (avg_opt_power - avg_opt_power[-1])[:-1],
            '-+',
            c=color,
            label=f'K = {n_users}'
        )

    avg_rate_ax.legend()
    avg_rate_ax.set_xlabel('iteration')
    avg_rate_ax.set_ylabel('Average achieved rate')

    constraints_ax.legend()
    constraints_ax.set_xlabel('iteration')
    constraints_ax.set_ylabel(
        f'percentage of users within {round((1 - RATE_THREASHOLD) * 100)} % of their rate constraint'
    )

    avg_power_ax.hlines(
        ANTENNA_POWER, 0,
        N_ITERATIONS + 1,
        ls='--',
        label='antenna power'
    )
    avg_power_ax.legend()
    avg_power_ax.set_xlabel('iteration')
    avg_power_ax.set_ylabel('Average power')

    users_fig.tight_layout(pad=1.0)
    users_fig.savefig(pgf_fig.joinpath('Q1_user_distribution.pgf'))
    users_fig.savefig(png_fig.joinpath('Q1_user_distribution.png'), dpi=600)

    avg_rate_fig.savefig(pgf_fig.joinpath('Q3_average_rate.pgf'))
    avg_rate_fig.savefig(png_fig.joinpath('Q3_average_rate.png'), dpi=600)

    penalty_ax.legend()
    penalty_ax.set_xlabel('penalty factor $V$')
    penalty_ax.set_ylabel('$p - p^*$')

    penalty_fig.savefig(pgf_fig.joinpath('Q3_power_vs_penalty.pgf'))
    penalty_fig.savefig(png_fig.joinpath('Q3_power_vs_penalty.png'), dpi=600)

    constraints_fig.savefig(pgf_fig.joinpath(
        'Q3_proportion_constraints_met.pgf'
    ))
    constraints_fig.savefig(png_fig.joinpath(
        'Q3_proportion_constraints_met.png'
    ), dpi=600)
    avg_power_fig.savefig(pgf_fig.joinpath('Q3_average_power.pgf'))
    avg_power_fig.savefig(png_fig.joinpath('Q3_average_power.png'), dpi=600)


################################################################################
# QUESTION 3                                                                   #
################################################################################
P = 20  # Base station power (in W)
GAMMA = 10**((10 - 30) / 10)  # SNR constraint
N = 5  # Max queue length
R = 30  # Data rate (in packet/s)
PKT_ARRIVAL_RATE = .7  # arrival rate of each user rate (in bits/s)


def tdma_factory(path_loss: np.ndarray, n_users: int):
    def transition_proba(target: np.ndarray, state: np.ndarray, scheduled: int) -> np.ndarray:
        """Evaluate the transistion prbability from a given state to a given
        target state, given an action"""

        # Probability of having a "good" channel
        transmit_proba = 1 - np.exp(
            - FADING_RATE * GAMMA * NOISE_POWER / (P * path_loss[scheduled])
        )

        # If the user's channel allowed them to transmit
        # The arrivals
        delta = target - state
        delta[scheduled] = target[scheduled] - \
            max(state[scheduled] - R, 0)

        # The probability of the transitions leading to a valid state (ie no
        # overflow)
        valid_proba = np.prod([
            bernoulli.pmf(delta[j], PKT_ARRIVAL_RATE)
            for j in range(n_users) if state[j] + delta[j] < N
        ])

        # The probability of the transitions leading to overflow
        overflow_proba = np.prod([
            bernoulli.pmf(1, PKT_ARRIVAL_RATE)
            for j in range(n_users) if state[j] + delta[j] < N
        ])

        proba_knowing_transmit = valid_proba * overflow_proba

        # The user's channel was too low
        # The arrivals
        delta = target - state

        # The probability of the transitions leading to a valid state (ie no
        # overflow)
        valid_proba = np.prod([
            bernoulli.pmf(delta[j], PKT_ARRIVAL_RATE)
            for j in range(n_users) if state[j] + delta[j] < N
        ])

        # The probability of the transitions leading to overflow
        overflow_proba = np.prod([
            bernoulli.pmf(1, PKT_ARRIVAL_RATE)
            for j in range(n_users) if state[j] + delta[j] < N
        ])

        proba_knowing_no_transmit = valid_proba * overflow_proba

        return transmit_proba * proba_knowing_transmit \
            + (1 - transmit_proba) * proba_knowing_no_transmit

    def reachable_states(state: np.ndarray, scheduled: int) -> list[np.ndarray]:
        """Generate the states reachable from on state, given an action"""
        delta = cartesian(2, n_users)

        # Assuming the user does not transmit
        reachable_no_transmit = state + delta

        # Assuming the user does transmit
        reachable_transmit = state + delta
        reachable_transmit[:, scheduled] = max(
            0, state[scheduled] - R) + delta[:, scheduled]

        # Filter reachable states to avoid overflow
        out = np.concatenate([reachable_no_transmit, reachable_transmit])
        out = out[np.all(out <= N, axis=1)]

        return out

    def reward(state: np.ndarray, scheduled: np.ndarray) -> float:
        """Average reward of following a given action from a given state"""

        # Sum of the queues of the users that were not scheduled
        non_scheduled_sum = np.sum(state[np.arange(n_users) != scheduled])

        # Average number of arrivals
        avg_arrivals = n_users * PKT_ARRIVAL_RATE

        # Average queue len for scheduled user at next state
        # Probability of having a "good" channel
        transmit_proba = 1 - np.exp(
            - FADING_RATE * GAMMA * NOISE_POWER / (P * path_loss[scheduled])
        )
        avg_next_queue = transmit_proba * max(0, state[scheduled] - R) \
            + (1 - transmit_proba) * state[scheduled]

        return - (non_scheduled_sum + avg_arrivals + avg_next_queue)

    def belman_operator(value: np.ndarray, state: np.ndarray, scheduled: int, gamma: float) -> float:
        """Compute the value of the bellman operator over one (action, state)
        tuple"""

        return reward(state, scheduled) + gamma * np.sum([
            transition_proba(next_state, state, scheduled) *
            value[state2int(next_state)]
            for next_state in reachable_states(state, scheduled)
        ])

    def state2int(state):
        n_users = state.shape[0]
        multiplier = (N + 1) ** np.arange(n_users)

        return np.sum(multiplier * state)

    def int2state(state_id):
        powers = (N + 1) ** np.arange(n_users)
        state = np.zeros(n_users, dtype=int)
        reminder = state_id

        for i in reversed(range(n_users)):
            state[i] = reminder // powers[i]
            reminder %= powers[i]

        return state

    return transition_proba, reachable_states, int2state, state2int, reward, belman_operator


def value_iteration(problem, n_users: int, n_iteration: int, gamma: float):
    transition_proba, reachable_states, int2state, state2int, reward, belman_operator = problem

    n_states = (N + 1)**n_users
    value = np.ones(n_states)

    for i in range(n_iteration):

        for state_id in tqdm.tqdm(range(n_states), desc=f'step {i}'):
            value[state_id] = np.max([
                belman_operator(value, int2state(state_id), scheduled, gamma)
                for scheduled in range(n_users)
            ])

    return value


def question_3():
    N_USERS = 3
    N_REALIZATIONS = 1_000
    DISCOUNT_FACTOR = .5
    N_RL_ITERATIONS = 1

    # Generate the cell
    users = generate_users(N_USERS, 1)
    distance = np.sqrt(users[:, 0]**2 + users[:, 1]**2)
    pathloss = 10**(-12.8 + -3.76 * np.log10(distance))

    # Initialize the training
    tdma = tdma_factory(pathloss, N_USERS)
    transition_proba, reachable_states, int2state, state2int, reward, belman_operator = tdma

    n_iterations_values = [1, 2, 5, 15]
    discount_values = [.1, .5]

    for n_iterations in n_iterations_values:
        for discount_factor in discount_values:
            # Execute value iteration
            optimal_value = value_iteration(
                tdma,
                N_USERS,
                n_iterations,
                discount_factor
            )

            # Evaluate value iteration over the channel
            queue = np.zeros(N_USERS, dtype=int)
            queue_len = np.zeros(N_REALIZATIONS)

            for i in tqdm.tqdm(range(N_REALIZATIONS), desc="Simulation"):
                channel = generate_channel(users, FADING_RATE)
                arrivals = np.random.binomial(
                    1, PKT_ARRIVAL_RATE, size=N_USERS)

                # Greedy policy with optimal value
                scheduled = np.argmax([
                    belman_operator(
                        optimal_value,
                        queue,
                        scheduled,
                        discount_factor
                    )
                    for scheduled in range(N_USERS)
                ])

                if P * channel[scheduled] / NOISE_POWER > GAMMA:  # Can transmit
                    queue[scheduled] = max(0, queue[scheduled] - R)

                queue = np.minimum(queue + arrivals, N)
                queue_len[i] = np.sum(queue)

            pl.plot(
                queue_len.cumsum() / np.arange(1, N_REALIZATIONS+1),
                label=f'$n={n_iterations}, \gamma={discount_factor}$'
            )

    pl.legend()
    pl.xlabel('time slot')
    pl.ylabel('average total queue length')
    pl.savefig(pgf_fig.joinpath('average_total_queue_length.pgf'))
    pl.savefig(png_fig.joinpath('average_total_queue_length.png'), dpi=600)


if __name__ == '__main__':
    question_1_3()
    # question_3()
