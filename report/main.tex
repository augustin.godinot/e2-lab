\documentclass[a4paper]{color-report}

\usepackage[utf8]{inputenc}		% Encode characters into latex's representation
\usepackage[T1]{fontenc}		% For font encoding
\usepackage[english]{babel} 	% Format text according to language convention
\usepackage{import}				% For advanced imports (relative, no doubles ...)
\usepackage{todonotes}			% For nice TODOs
\usepackage{biblatex}			% For bibliography
\usepackage{csquotes}			% Needed by biblatex polyglossia
\usepackage{graphics}			% To display pictures
\usepackage{bm}					% For bold math caracters
\usepackage{subcaption}			% For mutliple figures in one float
\usepackage{optidef}
\usepackage{grodino}			% custom package with custom math helpers


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STYLES                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\definecolor{black}{RGB}{0, 0, 0}
\definecolor{black-light}{RGB}{31, 30, 33}
\definecolor{primary}{RGB}{156, 0, 60}
\definecolor{accent-1}{RGB}{252, 135, 58}
\definecolor{accent-2}{RGB}{0, 160, 130}
\definecolor{accent-2-light}{RGB}{140, 190, 170}
\definecolor{accent-3}{RGB}{250, 100, 230}
\definecolor{accent-3-light}{RGB}{200, 160, 190}

\mainLogo{logos/CS-round-monochrome.png}
\secondaryLogo{logos/upsaclay.png}

\captionsetup{
	justification=justified, 
	width=.9\textwidth
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% METADATA                                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Mandatory fields
\title{Stochastic Optimization Techniques}
\author{Charbel Bou-Chaaya \\ Augustin Godinot}
\date{\today}
\subtitle{E2 - Network Optimization}
\reportType{LAB REPORT}
\info{}{M2 SAR \\ Advanced Wireless \\ Communications Systems}
\info{Contact}{%
    Augustin Godinot\hfill \href{mailto:augustin.godinot@student-cs.fr}{augustin.godinot@student-cs.fr} \\
    Charbel Bou-Chaaya\hfill \href{mailto:charbel.bou-chaaya@student-cs.fr}{charbel.bou-chaaya@student-cs.fr} \\
}
\info{Teacher}{%
    Mohammad Assaad \hfill \href{mailto:mohamad.assaad@centralesupelec.fr}{mohamad.assaad@centralesupelec.fr}
}


\begin{document}

\maketitle
%\tableofcontents
% \listoffigures
% \listoftables

\section{Lyapunov Optimization in TDMA and OFDMA systems}

\paragraph{1.} After generating K uniformly distributed users in the cell,
\autoref{fig:users} shows their positions.

\begin{figure}[H]
    \centering
    \scalebox{0.65}{\input{./figures/Q1_user_distribution.pgf}}
    \caption{Users' distribution in the circular cell.}
    \label{fig:users}
\end{figure}

\paragraph{2.} The optimization problem to solve is:
\begin{mini*}|l|
    {p_i, \forall i}
    {\lim_{T \to \infty} \frac{1}{T} \sum_{t=0}^{T-1} \sum_{i=1}^{K} \E{p_i(t)}}
    {}{}
    \addConstraint{\lim_{T \to \infty} \frac{1}{T} \sum_{t=0}^{T-1} \E{
            \log_2 \left(1+\frac{p_i(t)g_i(t)}{\sigma^2}\right)
        } \geq \bar{R}_i \qquad i=1,\dots,K}
    \addConstraint{p_i(t) \leq P \qquad i=1,\dots,K}
\end{mini*}
The first $K$ inequality constraints define the stability constraints for the
virtual queues: $Q_i(t+1) = \max\left(0, Q_i(t) + \bar{R}_i - \log_2
    \left(1+\frac{p_i(t)g_i(t)}{\sigma^2}\right)\right)$. The second $K$ constraints
define the set of actions: an action is the allocation of a certain amount of
power to one user in each time slot. The Drift-Plus-Penalty algorithm, with
penalty $V$, chooses the action, at each slot, that solves the following
problem:
\begin{mini*}|l|
    {p_i, \forall i}
    {V \sum_{i=1}^{K} p_i(t) + \sum_{i=1}^K Q_i(t) \left(\bar{R}_i - \log_2
    \left(1+\frac{p_i(t)g_i(t)}{\sigma^2}\right)\right)}
    {}{}
    \addConstraint{p_i(t) \leq P \qquad i=1,\dots,K}
\end{mini*}
We know that at each time slot, only one user can be active in a TDMA system.
Then to solve this minimization problem, we start by solving the following $K$
problems, where in each one, we schedule user $i$ and find the corresponding
power $p_i(t)$ that solves this new problem: $i=1,\dots,K$
\begin{mini*}|l|
    {p_i}
    {V p_i(t) - Q_i(t) \log_2 \left(1+\frac{p_i(t)g_i(t)}{\sigma^2}\right)}
    {}{}
    \addConstraint{p_i(t) \leq P}
\end{mini*}
Finally, the scheduled user in this slot, is the one that minimizes the
objective function. Each one of these problems verifies the LCQ condition since
$p_i(t)-P$ is affine. The Lagrangian corresponding to each problem is:
$\mathcal{L}(p_i,\lambda_i) = V p_i(t) - Q_i(t) \log_2
    \left(1+\frac{p_i(t)g_i(t)}{\sigma^2}\right) + \lambda_i \left(p_i(t) -
    P\right)$. Then we have the following KKT conditions for every $i=1,\dots,K$ :
\begin{align*}
    \frac{\partial \mathcal{L}}{\partial p_i} (p_i^\star,\lambda_i^\star) & = 0    &  & \tag*{(stationnarity)}           \\
    p_i^\star(t)                                                          & \leq P &  & \tag*{(primal feasibility)}      \\
    \lambda_i^\star                                                       & \geq 0 &  & \tag*{(dual feasibility)}        \\
    \lambda_i^\star \left(p_i^\star(t) - P\right)                         & = 0    &  & \tag*{(complementary slackness)}
\end{align*}
The first condition yields: $\lambda_i^\star = \frac{Q_i(t)g_i(t)}{p_i^\star(t)
        g_i(t) +\sigma^2}$. The last condition gives two cases: either $\lambda_i^\star
    = 0$ and $p_i^\star(t) = \frac{Q_i(t)}{V}-\frac{\sigma^2}{g_i(t)}$, otherwise
$p_i^\star(t) = P$. Then the power allocation that solves the problem if user
$i$ is scheduled can be expressed by: $p_i^\star(t) = \max\left(0,
    \min\left(\frac{Q_i(t)}{V}-\frac{\sigma^2}{g_i(t)}, P\right)\right)$. The
optimal action in this TDMA system is to schedule the user that minimizes the
objective function, with allocated power $p_i^\star(t)$.

\paragraph{3.} We present the results in the following figures.

\begin{figure}[H]
    \begin{subfigure}[t]{.5\textwidth}
        \centering
        \scalebox{.5}{\input{./figures/Q3_proportion_constraints_met.pgf}}
        % \input{./figures/Q3_proportion_constraints_met.pgf}
        \caption{Variation of the percentage of users having their rate close to the desired rate
            as a function of the iterations. We notice that after a sufficient amount of TDMA slots
            all users' rates are satisfied closely enough.}
        \label{fig:proportion_constraints_met}
    \end{subfigure}%
    %
    \begin{subfigure}[t]{.5\textwidth}
        \centering
        \scalebox{.5}{\input{./figures/Q3_average_rate.pgf}}
        % \input{./figures/Q3_average_rate.pgf}
        \caption{Average rate per user as a function of time.}
        \label{fig:average_rate}
    \end{subfigure}
\end{figure}

\begin{figure}
    \begin{subfigure}[t]{.48\textwidth}
        \centering
        \scalebox{0.48}{\input{./figures/Q3_average_power.pgf}}
        \caption{Evolution of the total average power in the network with time.}
        \label{fig:average_power}
    \end{subfigure}%
    \hfill%
    \begin{subfigure}[t]{.48\textwidth}
        \centering
        \scalebox{0.48}{\input{./figures/Q3_power_vs_penalty.pgf}}
        \caption{
            Optimality gap of the average achieved power $p$ after $100,000$
            iterations for different penalty values $V$. The optimal value $p^*$ was
            estimated using a high value of $V = 100$. As expected, in gap decrease
            linearly with slope $-1$ in log-log scale.
        }
        \label{fig:power_vs_penalty}
    \end{subfigure}
\end{figure}


\section{Markov Decision Process for user scheduling in TDMA}

\paragraph*{1.} The Markov decision process is defined as $\left(\mathcal{S, A,
        P, R}, \gamma\right)$:
\begin{itemize}
    \item $\mathcal{S}$: the set of states is the set of vectors of queue
          lengths of each user.
    \item $\mathcal{A}$: the set of actions is the set of users since the action
          is simply the choice of the scheduled user.
    \item $\mathcal{P}$: the state transition probabilities, developed in \textbf{2.}
    \item $\mathcal{R}$: the reward function $\mathcal{R}_q^a = \E{-\sum_{i=1}^K
                  Q_i(t+1) \under \pmb{Q}(t) = q, a(t) = a} $
    \item $\gamma$: the discount factor.
\end{itemize}
If the number of packets in the queue is unbounded, then the
number of states in the Markov chain is countably infinite. If we assume that
there are at most $N$ packets per queue, the number of states is $N_s = (N+1)^K$
where $K$ the number of users.

\paragraph*{2.} We assume that the users are independent. The state transition
probability is defined as $\mathcal{P}^a_{qq^\prime} = \Proba{\pmb{Q}(t+1) = q^\prime
        \under \pmb{Q}(t) = q, a(t) = a}$. We suppose at time $t$ the state of the chain is
$Q(t)$ and we let $Q^\prime(t)$ be the next state, where
\begin{equation*}
    \pmb{Q}(t) =
    \begin{bmatrix}
        n_1    \\
        \vdots \\
        n_K
    \end{bmatrix} \qquad
    \pmb{Q}^\prime(t) =
    \begin{bmatrix}
        n_1 + \Delta_1 \\
        \vdots         \\
        n_K + \Delta_K
    \end{bmatrix} \qquad
    \Delta_i \geq 0, \forall i
\end{equation*}
If we suppose that user $i$ is scheduled, meaning the action taken is $a = i$,
the new state can be reached from the previous one in two cases:
\begin{itemize}
    \item If user $i$ can transmit, the transition probability is:
          \begin{multline*}
              \Proba{
                  \begin{bmatrix}
                      n_1 + \Delta_1              \\
                      \vdots                      \\
                      \max(n_i - R, 0) + \Delta_i \\
                      \vdots                      \\
                      n_K + \Delta_K
                  \end{bmatrix}
                  \under
                  \begin{bmatrix}
                      n_1    \\
                      \vdots \\
                      n_i    \\
                      \vdots \\
                      n_K
                  \end{bmatrix}
                  , a = i
              } = \Proba{\SNR_i \geq \gamma}
              \left(\prod_{\substack{j = 1 \\ n_j + \Delta_j < N}}^{K} \Proba{A_j = \Delta_j}\right) \\
              \left(\prod_{\substack{j = 1 \\ n_j + \Delta_j \geq N}}^{K} \sum_{k = N - n_j}^{+ \infty} \Proba{A_j = k} \right)
          \end{multline*}
    \item If user $i$ cannot transmit:
          \begin{equation*}
              \Proba{
                  \begin{bmatrix}
                      n_1 + \Delta_1 \\
                      \vdots         \\
                      n_i + \Delta_i \\
                      \vdots         \\
                      n_K + \Delta_K
                  \end{bmatrix}
                  \under
                  \begin{bmatrix}
                      n_1    \\
                      \vdots \\
                      n_i    \\
                      \vdots \\
                      n_K
                  \end{bmatrix}
                  , a = i
              } =
              \Proba{\SNR_i < \gamma}
              \left(\prod_{\substack{j = 1 \\ n_j + \Delta_j < N}}^{K} \Proba{A_j = \Delta_j}\right)
              \left(\prod_{\substack{j = 1 \\ n_j + \Delta_j \geq N}}^{K} \sum_{k = N - n_j}^{+ \infty} \Proba{A_j = k} \right)
          \end{equation*}
\end{itemize}
The first term in both probability expression determines if the transmitted
packets are received correctly. It only depends on the fading $h_i(t)$ of the
active user, since the users are assumed to be fixed in the cell ($l_i$ is
constant). Therefore:
\begin{align*}
    \Proba{\SNR_i < \gamma} & = \Proba{h_i < \frac{\gamma \sigma^2}{P l_i}}               \\
                            & = \exp \left(-\lambda \frac{\gamma \sigma^2}{P l_i} \right)
\end{align*}
and
\begin{equation*}
    \Proba{\SNR_i \geq \gamma} = 1 - \exp \left(-\lambda \frac{\gamma \sigma^2}{P l_i} \right)
\end{equation*}
The second term accounts for the arrived packets for the unscheduled users when
their queues are not full, and the third term is for the full queues. In the
Poisson arrivals case:
\begin{equation*}
    \Proba{A_j = \Delta_j} = \frac{\lambda_j^{\Delta_j}}{\Delta_j!} e^{-\lambda_j}
\end{equation*}
In the simpler Bernoulli arrivals case ($A_j \sim \text{Bern}(p)$ and $\Delta_j
    \in \{0, 1\}$), we change the upper bound of the summation in the third term to
$1$. Finally, the transition probability is the sum of the probabilities in the
two cases. We note that we cannot factor the last two terms because the arrivals
considered in the two cases $(\pmb{A})_j$ are different.

\paragraph{3.} Before implementing the value iteration algorithm, we develop the
reward function as an expectation over the arrivals $\pmb{A}$ and the channel
realizations $\pmb{H}$:
\begin{align*}
    \mathcal{R}_q^a & = \E[\pmb{H}, \pmb{A}]{-\sum_{i=1}^K Q_i(t+1) \under \pmb{Q}(t) = q, a(t) = a} \\
                    & = \E[\pmb{H}, \pmb{A}]{-\left(\sum_{\substack{i=1                              \\ i \neq a}}^K Q_i(t) + A_i(t)\right) - Q_a(t+1) \under \pmb{Q}(t) = q, a(t) = a} \\
                    & = \left(-\sum_{\substack{i=1                                                   \\ i \neq a}}^K Q_i(t) - \E[A_i]{A_i(t)}\right) - \E[h_a, A_a]{Q_a(t+1)} &&(\pmb{H} \perp \pmb{A})
\end{align*}
We know that:
\begin{equation*}
    \E[h_a, A_a]{Q_a(t+1)}  = \Proba{\SNR_i < \gamma} Q_a(t) + \Proba{\SNR_i \geq \gamma} \max(Q_a(t) - R, 0) + \E[A_a]{A_a(t)}
\end{equation*}
and $\sum_{i=1}^K \E[A_i]{A_i(t)} = K p$, then finally
\begin{equation*}
    \mathcal{R}_q^a =
    \sum_{\substack{i=1 \\ i \neq a}}^K Q_i(t) + K p
    + \exp \left(-\lambda \frac{\gamma \sigma^2}{P l_i} \right) Q_a(t)
    + \left(1 - \exp \left(-\lambda \frac{\gamma \sigma^2}{P l_i}\right)\right) \max(Q_a(t) - R, 0)
\end{equation*}
We applied the value iteration on a network of $K = 3$ users with queue buffers
of $N = 5$ packets each, starting from an initial value function vector. After
that, we tested the obtained greedy policy from a different number of iterations
$n$, and different discount factors $\gamma$. \autoref{fig:average_total_queue_length} shows the
resulting average total queue lengths for 1000 time slots.

\begin{figure}
    \centering
    \input{./figures/average_total_queue_length.pgf}
    \caption{Moving average of the sum of the queue lengths as a function of
        time. The obtained policy is able to avoid having all the queues
        overflowed. Yet, the number of iterations and the discount factor do not
        seem to have a meaningful impact on the solution. This could mean that
        $15$ iterations are not sufficient to reach convergence.}
    \label{fig:average_total_queue_length}
\end{figure}

\end{document}